import uuid
from flask import Flask, jsonify, request
from flasgger import Swagger, swag_from

users = [
    {"id": "ae0487f3-3055-4b1a-b4bd-b51e415db087", "name": "Max Mustermann"},
    {"id": "af8cd92a-9275-436b-b4b1-b0691f2943cb", "name": "John Doe"}
]

lists = [
    {"id": "258ef569-788a-4327-95ce-329e4ab30e68", "name": "Einkaufsliste", "entries": [], "user": "ae0487f3-3055-4b1a-b4bd-b51e415db087"},
    {"id": "7a5e3053-0fc0-4917-b3c7-4d3f4a9880f6", "name": "ToDo-Liste", "entries": [
        {"id": "16702ea3-ce87-43a3-8bf9-5f61e35c3896", "name": "Küche aufräumen", "description": "Geschirrspüler anstellen"}
    ], "user": "af8cd92a-9275-436b-b4b1-b0691f2943cb"}
]

app = Flask(__name__)
swagger = Swagger(app)

# ToDo-List
@app.route('/todo-list/<listid>/', methods=['GET'])
@swag_from('doc/todo-list/get.yml')
def getToDoList(listid):
    return jsonify(searchForId(lists, listid))

@app.route('/todo-list/<listid>/', methods=['DELETE'])
@swag_from('doc/todo-list/delete.yml')
def deleteToDoList(listid):
    deleteById(lists, listid)
    return 'success'

@app.route('/todo-list', methods=['POST'])
@swag_from('doc/todo-list/create.yml')
def createToDoList():
    request_data = request.get_json()

    todo_list = {
        "id": str(uuid.uuid4()),
        "name": request_data['name'],
        "entries": [],
        "user": request_data['user']
    }
    lists.append(todo_list)
    return todo_list

# Entries
@app.route('/todo-list/<listid>/entry', methods=['POST'])
@swag_from('doc/entry/create.yml')
def createEntry(listid):
    request_data = request.get_json()

    entry = {
        "id": str(uuid.uuid4()),
        "name": request_data['name'],
        "description": request_data['description']
    }

    listObject = searchForId(lists, listid)
    listObject["entries"].append(entry)
    return listObject

@app.route('/todo-list/<listid>/entry/<entryid>', methods=['PUT'])
@swag_from('doc/entry/edit.yml')
def editEntry(listid, entryid):
    listObject = searchForId(lists, listid)
    entryObject = searchForId(listObject["entries"], entryid)
    request_data = request.get_json()

    entryObject["name"] = request_data['name']
    entryObject["description"] = request_data['description']
    return entryObject

@app.route('/todo-list/<listid>/entry/<entryid>', methods=['DELETE'])
@swag_from('doc/entry/delete.yml')
def deleteEntry(listid, entryid):
    listObject = searchForId(lists, listid)
    deleteById(listObject["entries"], entryid)
    return "success"

# User
@app.route('/users', methods=['GET'])
@swag_from('doc/user/get_all.yml')
def getAllUsers():
    return jsonify(users)

@app.route('/user', methods=['POST'])
@swag_from('doc/user/create.yml')
def createUser():
    request_data = request.get_json()

    user = {
        "id": str(uuid.uuid4()),
        "name": request_data['name']
    }
    users.append(user)
    return jsonify(user)

@app.route('/user/<userid>', methods=['DELETE'])
@swag_from('doc/user/delete.yml')
def deleteUser(userid):
    deleteById(users, userid)
    return 'success'

if __name__ == '__main__':
 app.run(host='0.0.0.0', port=5000)

# Misc Functions
def searchForId(dict, Id):
    for object in dict:
        if object['id'] == Id:
            return object

def deleteById(dict, Id):
    for object in dict:
        if object['id'] == Id:
            dict.remove(object)